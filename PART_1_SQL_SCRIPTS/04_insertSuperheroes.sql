INSERT INTO superhero (superhero_id, superhero_name, superhero_alias, superhero_origin) VALUES (1, 'Batman', 'Dark', 'Arkham');
INSERT INTO superhero (superhero_id, superhero_name, superhero_alias, superhero_origin) VALUES (2, 'Superman', 'Super', 'Krypton');
INSERT INTO superhero (superhero_id, superhero_name, superhero_alias, superhero_origin) VALUES (3, 'Deadpool', 'Dead', 'Canada');
