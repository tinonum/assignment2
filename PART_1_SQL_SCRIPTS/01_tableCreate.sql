CREATE TABLE superhero (
	superhero_id int PRIMARY key,
	superhero_name varchar(50) NOT NULL,
	superhero_alias varchar(50) NOT NULL,
	superhero_origin varchar(50) NOT NULL
);

CREATE TABLE assistant (
	assistant_id int PRIMARY key,
	assistant_name varchar(50) NOT NULL
);

CREATE TABLE power (
	power_id int PRIMARY KEY,
	power_name varchar(50) NOT NULL,
	power_description varchar (150) NOT NULL
);