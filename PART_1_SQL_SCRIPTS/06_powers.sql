INSERT INTO power (power_id, power_name, power_description) VALUES (1, 'invisible', 'Hero can invisible');
INSERT INTO power (power_id, power_name, power_description) VALUES (2, 'Flying', 'Hero can fly');
INSERT INTO power (power_id, power_name, power_description) VALUES (3, 'Laser eyes', 'Hero has laser eyes');
INSERT INTO power (power_id, power_name, power_description) VALUES (4, 'Fire', 'Hero can spit fire');

INSERT INTO hero_power (superhero_id, power_id) VALUES (1,3);
INSERT INTO hero_power (superhero_id, power_id) VALUES (1,4);
INSERT INTO hero_power (superhero_id, power_id) VALUES (2,1);
INSERT INTO hero_power (superhero_id, power_id) VALUES (2,2);
INSERT INTO hero_power (superhero_id, power_id) VALUES (3,1);
INSERT INTO hero_power (superhero_id, power_id) VALUES (3,4);

