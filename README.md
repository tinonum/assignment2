# DATABASE CREATION AND ACCESS

## Table of Contents

- [Description](#description)
- [Install](#install)
- [Usage](#usage)
- [Maintainer](#maintainer)
- [License](#license)

## Description

Assignment 2.1. SQL Scripts to create database


In the following folder: PART_1_SQL_SCRIPTS (Main) there are 8 parts of sql files which can be executed in the console of Spring Query. These scripts do as follows:

1. SQL script creates a table.
2. SQL script sets the relationship between two tables
3. SQL script does the same thing, it sets the relationship between two tables.
4. SQL script adds a value to a column in a certain table.
5. SQL script also adds a values to a column in a certain table.
6. SQL script also adds a values to a column in a certain table.
7. SQL script updates a certain column in a table.
8. SQL script deletes a certain column in a table.

Assignment 2.2. Handling data with JDBC. Database used = Chinook model of iTunes(mock)

The second part of the assignment was  to create functionalities for customers in the database that should be catered for: 

1. Reading all the customers in the database, this should display their: Id, first name, last name, country, postal code,
phone number and email.
2. Read a specific customer from the database (by Id), should display everything listed in the above point.
3. Read a specific customer by name. HINT: LIKE keyword can help for partial matches.
4. Return a page of customers from the database. This should take in limit and offset as parameters and make use
of the SQL limit and offset keywords to get a subset of the customer data. The customer model from above
should be reused.
5. Add a new customer to the database. You also need to add only the fields listed above (our customer object)
6. Update an existing customer.
7. Return the country with the most customers.
8. Customer who is the highest spender (total in invoice table is the largest).
9. For a given customer, their most popular genre (in the case of a tie, display both). Most popular in this context
means the genre that corresponds to the most tracks from invoices associated to that customer.


## Install

No installation needed, all dependencies are downloaded through gradle.

## Usage

Postgres database (Chinook).

Open project in Intellij and run. Requires Java 17.

## Maintainer

Tino Nummela | [@tinonum](https://gitlab.com/tinonum)

Ahmet Hilmi Terzi | [@hilmi46](https://gitlab.com/hilmi46)

## License

MIT © 2022 Tino Nummela, Ahmet Hilmi Terzi
