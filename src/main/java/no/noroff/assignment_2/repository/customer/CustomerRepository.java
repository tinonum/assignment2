package no.noroff.assignment_2.repository.customer;

import no.noroff.assignment_2.dataaccess.models.CustomerSpender;
import no.noroff.assignment_2.dataaccess.models.Customer;
import no.noroff.assignment_2.dataaccess.models.CustomerGenre;
import no.noroff.assignment_2.dataaccess.models.CustomerCountry;
import no.noroff.assignment_2.repository.CrudRepository;

import java.util.List;


public interface CustomerRepository extends CrudRepository<Customer, Integer> {

    /**
     * findByName() method finds customer by given parameter.
     * @param first_name
     * @return customer_id, first_name, last_name, country, postal_code, phone, email
     */
    Customer findByName(String first_name);

    /**
     * getPageOfCustomers() method return page of customers with given limit and offset.
     * Limit = amount, Offset = starting point
     * @param limit
     * @param offset
     * @return customer_id, first_name, last_name, country, postal_code, phone, email
     */
    List<Customer> getPageOfCustomers(int limit, int offset);

    /**
     * mostCustomers() method returns country with the most customers.
     * @return country
     */
    CustomerCountry mostCustomers();

    /**
     * bigSpender() method returns the customer with the highest sum of total.
     * @return customer_id, total_spent(SUM(total)
     */
    CustomerSpender bigSpender();

    /**
     * popularGenre() method returns the most popular genre of given customer.
     * In case of a tie, method displays both.
     * @param customer_id
     * @return customer_id, genre.name, occurrences(COUNT(genre.genre_id))
     */
    List<CustomerGenre> popularGenre(Integer customer_id);
}

