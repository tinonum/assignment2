package no.noroff.assignment_2.repository.customer;

import no.noroff.assignment_2.dataaccess.models.CustomerSpender;
import no.noroff.assignment_2.dataaccess.models.Customer;
import no.noroff.assignment_2.dataaccess.models.CustomerGenre;
import no.noroff.assignment_2.dataaccess.models.CustomerCountry;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CustomerRepositoryImpl implements CustomerRepository {
    private final String url;
    private final String username;
    private final String password;


    /**
     * Reads database configuration from application.properties via @Value
     * @param url
     * @param username
     * @param password
     */
    public CustomerRepositoryImpl(
            @Value("${spring.datasource.url}") String url,
            @Value("${spring.datasource.username}") String username,
            @Value("${spring.datasource.password}") String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }

    // Reads all the customers
    @Override
    public List<Customer> findAll() {
        String sql = "SELECT * FROM customer";
        List<Customer> customers = new ArrayList<>();
        try(Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = conn.prepareStatement(sql);
            ResultSet result = statement.executeQuery();
            while(result.next()) {
                Customer customer = new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")
                );
                customers.add(customer);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customers;
    }

    // Read customer by ID
    @Override
    public Customer findById(Integer customer_id) {
        Customer customer = null;
        String sql = "SELECT * FROM customer WHERE customer_id = ?";
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, customer_id);
            ResultSet result = statement.executeQuery();
            while(result.next()) {
                 customer = new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customer;
    }

    // Read customer by Name
    @Override
    public Customer findByName(String first_name) {
        Customer customer = null;
        String sql = "SELECT * FROM customer WHERE first_name = ?";
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, first_name);
            ResultSet result = statement.executeQuery();
            while(result.next()) {
                customer = new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customer;
    }

    // Return page of customers with limit and offset keywords
    @Override
    public List<Customer> getPageOfCustomers(int limit, int offset) {
        String sql = "SELECT * FROM customer LIMIT ? OFFSET ?";
        List<Customer> customers = new ArrayList<>();
        try(Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, limit);
            statement.setInt(2, offset);
            ResultSet result = statement.executeQuery();
            while(result.next()) {
                Customer customer = new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")
                );
                customers.add(customer);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customers;
    }

    // Insert new customer
    @Override
    public int insert(Customer object) {
        String sql = "INSERT INTO customer (customer_id, first_name, last_name, country, postal_code, phone, email) VALUES (?,?,?,?,?,?,?)";
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, object.customer_id());
            statement.setString(2, object.first_name());
            statement.setString(3, object.last_name());
            statement.setString(4, object.country());
            statement.setString(5, object.postal_code());
            statement.setString(6, object.phone());
            statement.setString(7, object.email());
            statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    // Update existing customer
    @Override
    public int update(Customer object) {
        String sql = "UPDATE customer SET first_name = ?, last_name = ?, country = ?, postal_code = ?, phone = ?, email = ? WHERE customer_id = ?";
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(7, object.customer_id());
            statement.setString(1, object.first_name());
            statement.setString(2, object.last_name());
            statement.setString(3, object.country());
            statement.setString(4, object.postal_code());
            statement.setString(5, object.phone());
            statement.setString(6, object.email());
            statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    // Return country with the most customers
    @Override
    public CustomerCountry mostCustomers() {
        CustomerCountry mostCustomers = null;
        String sql = "SELECT country, COUNT (country) FROM customer GROUP BY country ORDER BY country";
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = conn.prepareStatement(sql);
            ResultSet result = statement.executeQuery();
            while(result.next()) {
                mostCustomers = new CustomerCountry(
                        result.getString("country")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return mostCustomers;
    }

    // Return the BIG spender customer
    @Override
    public CustomerSpender bigSpender() {
        CustomerSpender bigSpender = null;
        String sql = "SELECT customer_id, SUM(total) AS total_spent FROM invoice GROUP BY customer_id ORDER BY total_spent ASC";
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = conn.prepareStatement(sql);
            ResultSet result = statement.executeQuery();
            while(result.next()) {
                bigSpender = new CustomerSpender(
                        result.getInt("customer_id"),
                        result.getString("total_spent")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return bigSpender;

    }

    // Get customer most popular genre
    @Override
    public List<CustomerGenre> popularGenre(Integer customer_id) {
        List<CustomerGenre> customerGenres = new ArrayList<>();
        String sql = "SELECT customer_id, genre.name, COUNT(genre.genre_id) AS occurrences FROM invoice\n" +
                "INNER JOIN invoice_line ON invoice.invoice_id = invoice_line.invoice_id\n" +
                "INNER JOIN track ON invoice_line.track_id = track.track_id\n" +
                "INNER JOIN genre ON track.genre_id = genre.genre_id\n" +
                "WHERE customer_id = ?\n" +
                "GROUP BY customer_id, genre.name, genre.genre_id ORDER BY occurrences DESC\n" +
                "FETCH FIRST ROWS WITH TIES";
        try(Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, customer_id);
            ResultSet result = statement.executeQuery();
            while(result.next()) {
                CustomerGenre customerGenre = new CustomerGenre(
                        result.getInt("customer_id"),
                        result.getString("name"),
                        result.getString("occurrences")
                );
                customerGenres.add(customerGenre);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customerGenres;
    }

    // Delete costumer by costumer_id
    @Override
    public int deleteById(Integer customer_id) {
        int affectedRows = 0;
        String sql = "DELETE FROM customer WHERE customer_id = ?";
        try(Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, customer_id);
            affectedRows = statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return affectedRows;
    }
}

