package no.noroff.assignment_2.repository;

import java.util.List;

public interface CrudRepository<T, ID> {
    /**
     * findAll() method read all the customers in the database.
     * @return customer_id, first_name, last_name, country, postal_code, phone, email
     */
    List<T> findAll();

    /**
     * findByID() method finds customer by given parameter.
     * @param customer_id
     * @return customer_id, first_name, last_name, country, postal_code, phone, email
     */
    T findById(ID customer_id);

    /**
     * insert() method inserts new customer in the database.
     * @param object (customer_id, first_name, last_name, country, postal_code, phone, email)
     * @return 0;
     */
    int insert(T object);

    /**
     * update() method updates existing customer in the database.
     * @param object (customer_id, first_name, last_name, country, postal_code, phone, email)
     * @return 0;
     */
    int update(T object);

    /**
     * deleteById() method deletes customer by given parameter.
     * @param customer_id
     * @return affectedRows;
     */
    int deleteById(ID customer_id);
}