package no.noroff.assignment_2.dataaccess.runners;


import no.noroff.assignment_2.dataaccess.models.Customer;
import no.noroff.assignment_2.repository.customer.CustomerRepository;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class PgRunner implements ApplicationRunner {
    private final CustomerRepository customerRepository;

    public PgRunner(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    /**
     * All the methods called and ready to be used.
     * @param args incoming application arguments
     */
    @Override
    public void run(ApplicationArguments args) {
        // Find all customers
        //System.out.println(customerRepository.findAll());

        // Find customer by ID
        //System.out.println(customerRepository.findById(42));

        // Find customer by name
        //System.out.println(customerRepository.findByName("Jack"));

        // Get page of customers
        //System.out.println(customerRepository.getPageOfCustomers(5, 2));

        // Insert customer
        //System.out.println((customerRepository.insert(new Customer(60, "First","Last", "Netherlands", "123456", "123-456-7890","email@email.fi"))));

        // Update customer
        //System.out.println(customerRepository.update(new Customer(60, "New First", "New Last", "New Netherlands", "New 123456", "New 123-456-7890","New email@email.fi")));

        // Get country with the most customers
        //System.out.println(customerRepository.mostCustomers());

        // Get the biggest spender
        //System.out.println(customerRepository.bigSpender());

        // Get the most popular genre by customer
        //System.out.println(customerRepository.popularGenre(12));

        // Delete customer by customer_id
        //System.out.println(customerRepository.deleteById(61));
    }
}
