package no.noroff.assignment_2.dataaccess.models;

public record CustomerCountry(String country) {
}
