package no.noroff.assignment_2.dataaccess.models;

public record CustomerGenre(int customer_id, String name, String occurrences) {
}
