package no.noroff.assignment_2.dataaccess.models;

public record CustomerSpender(int customer_id, String total_spent) {
}
